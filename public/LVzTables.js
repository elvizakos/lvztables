(function ( ) {
/**
 * @typedef {Object} ElementOffset
 * @property {number} left - Left offset position for element.
 * @property {number} top - Top offset position for element.
*/

	/**
	 * Function for checking if an element is within two points
	 * @param {HTMLElement} el - The element to check.
	 * @param {number} x1 - The first point X coord.
	 * @param {number} y1 - The first point Y coord.
	 * @param {number} x2 - The second point X coord.
	 * @param {number} y2 - The second point Y coord.
	 * return {boolean} True if the element is within or touching the imaginary box that these two points form, false otherwise.
	 */
	function isBetween ( el, x1, x2, y1, y2 ) {
		var o=getOffset(el),th=o.top+el.offsetHeight,lw=o.left+el.offsetWidth;
		return((o.top>=y1&&o.top<y2)||(o.top<y1&&th>=y1))&&((o.left>=x1&&o.left<x2)||(o.left<x1&&lw>=x1));
	}

	/**
	 * Returns an object with the offset position of an element.
	 * @param {HTMLElement} el - The element to get it's offset position.
	 * @return {ElementOffset} Element's offset position.
	 */
	function getOffset ( el ) {

		var ret={
			left:0,
			top:0
		},o=el;

		while ( o&&o.nodeName!='#document' ) {
			ret.left+=o.offsetLeft;
			ret.top+=o.offsetTop;
			o=o.offsetParent;
		}

		return ret;
	}

	/**
	 * Function that checks if an element is in view.
	 * @param {HTMLElement} el - The elemetn to check.
	 * @return {boolean} True if the element is in view, false otherwise.
	 */
	function isInView ( el ) {
		if ( el ) return isBetween(el,pageXOffset,pageXOffset+innerWidth,pageYOffset,pageYOffset+innerHeight);
		return false;
	}

	/**
	 * Class name of tables that the "tables" object will autoload.
	 * @type {string}
	 */
	var className = 'lvzTable';

	/**
	 * Array of all tables objects.
	 * @type {Array}
	 */
	var tableslist = [];

	/**
	 * Constructs object that controls the tables.
	 *
	 * @constructor
	 * @param {HTMLElement} el - The table element
	 */
	var tables = function ( el ) {

		this.id = tableslist.push(this);

		this.props = {
			onColumn : null,
			onColumnNo : false,
			reverseOrder : true
		};

		this.staticHeader = false;
		this.tableHeader = false;
		this.tableElement = false;

		this.buildStaticHeader = function () {
			if ( this.tableElement !== false &&
				 this.tableElement !== null &&
				 this.tableElement.tHead !== null &&
				 this.tableElement.tHead.lastElementChild !== null ) {

				this.staticHeader=document.createElement('table');
				this.staticHeader.dataTableObject=this;
				this.staticHeader.style.position = 'absolute';
				this.staticHeader.style.display = 'none';
				this.staticHeader.style.margin = '0';
				this.staticHeader.style.backgroundColor = '#fff';
				this.tableElement.parentElement.appendChild(this.staticHeader);
				var tmptbody = document.createElement('thead');
				tmptbody.dataTableObject=this;
				this.staticHeader.appendChild(tmptbody);
				var tmptr = document.createElement('tr');
				tmptr.dataTableObject=this;
				tmptbody.appendChild(tmptr);
				this.tableHeader = this.tableElement.tHead.lastElementChild;
				for ( var i = 0; i < this.tableElement.tHead.lastElementChild.children.length; i++ ) {
					var tmpth = document.createElement('th');
					tmpth.dataTableObject=this;
					tmpth.dataStaticHeaderColumn = true;
					tmpth.dataOriginalTableColumn = this.tableElement.tHead.lastElementChild.children[i];
					this.tableElement.tHead.lastElementChild.children[i].dataStaticHeaderColumn = tmpth;
					tmpth.innerHTML = this.tableElement.tHead.lastElementChild.children[i].innerHTML;

					this.tableElement.tHead.lastElementChild.children[i].className += ' sortableHeaderColumn';
					this.tableElement.tHead.lastElementChild.children[i].dataTableObject=this;
					this.tableElement.tHead.lastElementChild.children[i].addEventListener ( 'click' , function ( e ) {
						e.target.dataTableObject.sortColumn ( e.target ) ;
					} );

					tmpth.className = 'sortableHeaderColumn';
					tmpth.style.width = this.tableElement.tHead.lastElementChild.children[i].offsetWidth + 'px';
					tmpth.style.margin='0'; tmpth.style.padding='0';
					tmpth.addEventListener ( 'click' , function ( e ) {
						e.target.dataTableObject.sortColumn ( e.target ) ;
					} );
					tmptr.appendChild(tmpth);
				}
			}
			return this;
		};

		this.placeStaticHeader = function () {
			var o = getOffset(this.tableHeader),
				w = this.tableHeader.offsetWidth;
			this.staticHeader.style.display = 'block';
			this.staticHeader.style.margin = '0px';
			this.staticHeader.style.padding = '0px';
			this.staticHeader.style.width = w + 'px';

			// position: Fixed (better for vertical scrolling)
			this.staticHeader.style.position = 'fixed';
			this.staticHeader.style.top = '0px';
			this.staticHeader.style.left = ( o.left - window.scrollX ) + 'px';

			// position: absolute (better for horizontal scrolling)
			// this.staticHeader.style.position = 'absolute';
			// this.staticHeader.style.top = window.scrollY + 'px';
			// this.staticHeader.style.left = o.left + 'px';

			return this;
		};

		this.hideStaticHeader = function () {
			this.staticHeader.style.display = 'none';
			return this;
		};

		this.addTableElement = function ( el ) {

			this.tableElement = el;
			this.tableElement.dataTableObject = this;
			this.buildStaticHeader();

			return this;
		};

		this.sortColumn = function ( colElement ) {

			if ( typeof colElement.dataOriginalTableColumn != 'undefined' )
				colElement=colElement.dataOriginalTableColumn;

			var tmpel = colElement, pos = 0;
			while ( tmpel.previousElementSibling !== null ) {
				tmpel = tmpel.previousElementSibling;
				pos ++;
			}

			if ( this.props.onColumn !== null ) {
				this.props.onColumn.className = this.props.onColumn.className
					.replace( /orderColumn/im, '')
					.replace( /reverseOrder/im, '');

				this.props.onColumn.dataStaticHeaderColumn.className = this.props.onColumn.dataStaticHeaderColumn.className
					.replace( /orderColumn/im, '')
					.replace( /reverseOrder/im, '');
			}

			if ( this.props.onColumn === colElement )
				this.props.reverseOrder = !this.props.reverseOrder;
			else this.props.reverseOrder = false;

			this.props.onColumn = colElement;
			this.props.onColumn.className += ' orderColumn';
			this.props.onColumn.dataStaticHeaderColumn.className  += ' orderColumn';
			this.props.onColumnNo = pos;

			var tbody = this.tableElement.tBodies[0];
			var trlist = [];
			for ( var i = 0; i < tbody.children.length; i ++ ) {
				trlist.push ( {
					"element" : tbody.children[i],
					"text" : tbody.children[i].children[pos].innerText
				} );
			}

			if(this.props.reverseOrder) {
				this.props.onColumn.className += ' reverseOrder';
				this.props.onColumn.dataStaticHeaderColumn.className += ' reverseOrder';
				trlist.sort( function ( a, b ) {
					return 0-a.text.localeCompare(b.text);
				} );
			} else trlist.sort ( function ( a, b ) {
				return a.text.localeCompare(b.text);
			} );

			for ( i = 0; i < trlist.length; i ++ ) tbody.appendChild ( trlist[i].element );
			return this;
		};
	};

	window.addEventListener('load',function () {
		var tbls = document.getElementsByClassName(className);

		for ( var i = 0; i < tbls.length; i++ ) {

			switch ( tbls[i].nodeName ) {
			case 'TABLE':
				var tblobj = new tables();
				tblobj.addTableElement ( tbls[i] );
				break;

			case 'DIV':

			}
		}

	});

	window.addEventListener('scroll',function() {

		for ( var i = 0 ; i < tableslist.length; i++ ) {
			if ( isInView( tableslist[i].tableElement) &&
				 !isInView ( tableslist[i].tableElement.tHead ) ) {

				tableslist[i].placeStaticHeader();

			} else {

				tableslist[i].hideStaticHeader();

			}
		}

	});

	window.lvzTable = tables;
}) ( );
