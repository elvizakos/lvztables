# LVzTables #

Script for working with HTML tables.

Demo at https://elvizakos.gitlab.io/lvztables

## Table Object ##

### Table Object Methods ###

  * buildStaticHeader

    Create the static header for the table.

  * placeStaticHeader

    Place static header.

  * hideStaticHeader

    Hide static header.

  * addTableElement

    Link table element with current object.

  * sortColumn

    Sort data in table on the given column.

## HTML elements ##

### Table Main Element ###

#### Table Element Attributes ####

### DIV Main Element ###

#### DIV Main Element Attributes  ####

  * **source** or **data-source**
  * **header-from-row** or **data-header-from-row**
